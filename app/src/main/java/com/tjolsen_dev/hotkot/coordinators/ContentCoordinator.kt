package com.tjolsen_dev.hotkot.coordinators

import android.app.Activity
import android.content.Intent
import com.tjolsen_dev.hotkot.activities.DetailActivity
import com.tjolsen_dev.hotkot.models.HotKotMenuItem

class ContentCoordinator {

    fun showDetail(activity: Activity, item: HotKotMenuItem) {

        val intent = Intent(activity, DetailActivity::class.java)
        intent.putExtra("MENU_ITEM_EXTRA", item)

        activity.startActivity(intent)
    }
}