package com.tjolsen_dev.hotkot.coordinators

import android.app.Activity
import android.content.Intent
import android.net.Uri
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.tjolsen_dev.hotkot.viewModels.MenuViewModel

class DeepLinkCoordinator {

    fun routeLink(intent: Intent ,activity: Activity, menuViewModel: MenuViewModel) {

        FirebaseDynamicLinks.getInstance()
            .getDynamicLink(intent)
            .addOnSuccessListener(activity) { pendingDynamicLinkData ->

                var deepLink: Uri?

                if (pendingDynamicLinkData != null) {

                    deepLink = pendingDynamicLinkData.link
                    val destination: String = deepLink.toString().split("menuItem?=")[1]

                    when (destination) {
                        "0" -> ContentCoordinator().showDetail(activity, menuViewModel.menu[0])
                        "1" -> ContentCoordinator().showDetail(activity, menuViewModel.menu[1])
                    }
                }
            }.addOnFailureListener { exception ->

                print(exception.localizedMessage)
            }
    }
}