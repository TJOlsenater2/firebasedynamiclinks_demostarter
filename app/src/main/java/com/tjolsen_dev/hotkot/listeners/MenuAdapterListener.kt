package com.tjolsen_dev.hotkot.listeners

import android.app.Activity
import android.view.View
import android.widget.AdapterView
import com.tjolsen_dev.hotkot.coordinators.ContentCoordinator
import com.tjolsen_dev.hotkot.models.HotKotMenuItem
import com.tjolsen_dev.hotkot.viewModels.MenuViewModel

class MenuAdapterListener(val activity: Activity, val viewModel: MenuViewModel?): AdapterView.OnItemClickListener {

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        val item: HotKotMenuItem = parent?.getItemAtPosition(position) as HotKotMenuItem

        ContentCoordinator().showDetail(activity, item)
    }
}