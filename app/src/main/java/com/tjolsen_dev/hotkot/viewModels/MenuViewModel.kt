package com.tjolsen_dev.hotkot.viewModels

import android.app.Activity
import android.widget.AdapterView
import android.widget.ListView
import com.tjolsen_dev.hotkot.R
import com.tjolsen_dev.hotkot.adapters.MenuAdapter
import com.tjolsen_dev.hotkot.listeners.MenuAdapterListener
import com.tjolsen_dev.hotkot.models.HotKotMenuItem

class MenuViewModel(var menu: ArrayList<HotKotMenuItem>) {

    var menuAdapter: MenuAdapter? = null
    var menuAdapterListener: AdapterView.OnItemClickListener? = null

    fun setAdapter(activity: Activity) {

        menuAdapter = MenuAdapter(activity, menu)
        menuAdapterListener = MenuAdapterListener(activity, this)
        activity.findViewById<ListView>(R.id.menu_list).setOnItemClickListener(menuAdapterListener!!)
    }
}