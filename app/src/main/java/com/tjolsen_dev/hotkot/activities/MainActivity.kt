package com.tjolsen_dev.hotkot.activities
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.tjolsen_dev.hotkot.R
import com.tjolsen_dev.hotkot.coordinators.DeepLinkCoordinator
import com.tjolsen_dev.hotkot.utils.MenuHelper
import com.tjolsen_dev.hotkot.viewModels.MenuViewModel

class MainActivity : AppCompatActivity() {

    val menuViewModel = MenuViewModel(MenuHelper().makeMenu())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        title = getString(R.string.menu_title)

        menuViewModel.setAdapter(this)
        MenuHelper().showMenu(this, menuViewModel.menuAdapter!!, menuViewModel.menuAdapterListener!!)

        DeepLinkCoordinator().routeLink(intent, this, menuViewModel)
    }
}
