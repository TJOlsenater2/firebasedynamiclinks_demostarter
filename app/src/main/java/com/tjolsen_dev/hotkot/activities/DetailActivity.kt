package com.tjolsen_dev.hotkot.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.tjolsen_dev.hotkot.R
import com.tjolsen_dev.hotkot.models.HotKotMenuItem
import com.tjolsen_dev.hotkot.utils.MenuHelper

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        title = getString(R.string.details_title)

        MenuHelper().showItemDetails(
            this,
            intent.getSerializableExtra("MENU_ITEM_EXTRA") as HotKotMenuItem)
    }
}
