package com.tjolsen_dev.hotkot.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.tjolsen_dev.hotkot.R
import com.tjolsen_dev.hotkot.models.HotKotMenuItem

class MenuAdapter(private val context: Context, private val menuItems: ArrayList<HotKotMenuItem>): BaseAdapter() {

    override fun getCount(): Int {

        return menuItems.count()
    }

    override fun getItem(position: Int): Any {

        return menuItems[position]
    }

    override fun getItemId(position: Int): Long {

        return 0xc01a + position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val viewHolder: ViewHolder
        val menuItem: HotKotMenuItem = getItem(position) as HotKotMenuItem
        val view: View

        if (convertView == null) {

            view = LayoutInflater.from(context)
                .inflate(R.layout.adapter_menu_item, parent, false)

            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        }
        else  {

            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        viewHolder.imageView.setImageResource(menuItem.imageResource)
        viewHolder.titleTextView.text = menuItem.title

        return view
    }

    class ViewHolder(layout: View) {

        val imageView: ImageView = layout.findViewById(R.id.menu_item_image_view)
        val titleTextView: TextView = layout.findViewById(R.id.menu_item_title_text_view)
    }
}