package com.tjolsen_dev.hotkot.utils

import android.app.Activity
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import com.tjolsen_dev.hotkot.R
import com.tjolsen_dev.hotkot.adapters.MenuAdapter
import com.tjolsen_dev.hotkot.models.HotKotMenuItem

class MenuHelper {

    fun makeMenu(): ArrayList<HotKotMenuItem> {

        return arrayListOf(
            HotKotMenuItem(
                R.drawable.ic_chicken,
                "Chicken Hot Kot",
                "Description: Chicken in original broth with egg, turnip, and white rice."),
            HotKotMenuItem(
                R.drawable.ic_cow,
                "Beef Hot Kot",
                "Description: Beef in tomato broth with shrimp ball, udon noodle, and white spinach."),
            HotKotMenuItem(
                R.drawable.ic_sheep,
                "Lamb Hot Kot",
                "Description: Lamb in mushroom broth with fried tofu, fungus, and potato."),
            HotKotMenuItem(
                R.drawable.ic_pig,
                "Pork Hot Kot",
                "Description: Pork in curry broth with mini sausage, seaweed, and sweet corn.")
        )

    }

    fun showMenu(actvity: Activity, adapter: MenuAdapter, listner: AdapterView.OnItemClickListener) {

        val listView: ListView = actvity.findViewById(R.id.menu_list)
        listView.adapter = adapter
        listView.onItemClickListener = listner
    }

    fun showItemDetails(actvity: Activity, menuItem: HotKotMenuItem) {

        actvity.findViewById<ImageView>(R.id.detail_image_view).setImageResource(menuItem.imageResource)
        actvity.findViewById<TextView>(R.id.detail_title_text_view).text = menuItem.title
        actvity.findViewById<TextView>(R.id.detail_detail_text_view).text = menuItem.detail
    }
}