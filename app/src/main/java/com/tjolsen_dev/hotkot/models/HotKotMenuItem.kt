package com.tjolsen_dev.hotkot.models

import java.io.Serializable

class HotKotMenuItem(val imageResource: Int, val title: String, val detail: String): Serializable {
}