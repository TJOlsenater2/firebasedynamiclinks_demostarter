# Firebase Dynamic Links
### _A Starter Project_

This is a starter project intended to help developers become familiar with Firebase Dynamic Links. The purpose of this README is to help you pull down and work through the starter project. To help accomplish this purpose, the README is divided into two sections. The first section - _Set Up_ - gives instructions on how to pull down the repo and open the starter project. The second section, - _Walkthrough_ - provides a walkthrough tutorial on the structure of the starter project, as well as guidance on how to set up Firebase Dynamic Links.